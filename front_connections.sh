#!/bin/sh

chmod 400 ~/.ssh/id_rsa
sudo chmod 777 /home/adminfior/.ssh
sudo chmod 777 /home/adminfior/.ssh/known_hosts
sudo ssh-keyscan 10.10.2.4 >> ~/.ssh/known_hosts
sudo ssh-keyscan 10.10.2.5 >> ~/.ssh/known_hosts
sudo chmod 700 /home/adminfior/.ssh
sudo chmod 644 /home/adminfior/.ssh/known_hosts
ssh -p22 adminfior@10.10.2.4 "ls -la /home/adminfior"
ssh -p22 adminfior@10.10.2.5 "ls -la /home/adminfior"
